<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "alphabet".
 *
 * @property int $id
 * @property string $letter
 * @property string $description
 * @property string $imgURL
 * @property string $audioURL
 * @property string $bgColor
 */
class Alphabet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alphabet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['letter', 'description', 'imgURL', 'audioURL', 'bgColor'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'letter' => Yii::t('app', 'Letter'),
            'description' => Yii::t('app', 'Description'),
            'imgURL' => Yii::t('app', 'Img Url'),
            'audioURL' => Yii::t('app', 'Audio Url'),
            'bgColor' => Yii::t('app', 'Bg Color'),
        ];
    }
}
