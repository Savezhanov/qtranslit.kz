<?php

namespace frontend\controllers;

use frontend\models\Alphabet;

class AlphabetController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $alphabet = Alphabet::find()->all();
        return $this->render('index',array("alphabet" => $alphabet));
    }

}
