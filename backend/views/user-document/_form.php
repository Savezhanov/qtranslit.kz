<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model,'doc')->fileInput(); ?>

    <?/*= $form->field($model, 'uploaded_url')->textInput(['maxlength' => true]) */?>

    <?= $form->field($model, 'converted_url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
