<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Alphabet */

$this->title = Yii::t('app', 'Create Alphabet');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alphabet'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alphabet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
