<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "letters".
 *
 * @property int $id
 * @property string $old_upletter
 * @property string $new_upletter
 * @property string $old_lowletter
 * @property string $new_lowletter
 */

class Letters extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'letters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_lowletter', 'new_lowletter'], 'required'],
            [['old_upletter', 'new_upletter', 'old_lowletter', 'new_lowletter'], 'string', 'max' => 10],
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['old_upletter','new_upletter','old_lowletter','new_lowletter']; //Scenario Values Only Accepted
        /*$scenarios['update'] = ['old_upletter','new_upletter','old_lowletter','new_lowletter']; //Scenario Values Only Accepted*/
        return  $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'old_upletter' => 'Old Upletter',
            'new_upletter' => 'New Upletter',
            'old_lowletter' => 'Old Lowletter',
            'new_lowletter' => 'New Lowletter',
        ];
    }
}
