<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\Alphabet;

class AlphabetController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionListAlphabet(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $alphabet = Alphabet::find()->all();

        if( count($alphabet) > 0 ) {
            return array('status' => true, 'data' => $alphabet);
        }else {
            return array('status' => false, 'data' => 'No letters found.');
        }
    }

}
