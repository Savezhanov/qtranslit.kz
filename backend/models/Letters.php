<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "letters".
 *
 * @property int $id
 * @property string $old_upletter
 * @property string $new_upletter
 * @property string $old_lowletter
 * @property string $new_lowletter
 */
class Letters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'letters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_upletter', 'new_upletter', 'old_lowletter', 'new_lowletter'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'old_upletter' => Yii::t('app', 'Old Upletter'),
            'new_upletter' => Yii::t('app', 'New Upletter'),
            'old_lowletter' => Yii::t('app', 'Old Lowletter'),
            'new_lowletter' => Yii::t('app', 'New Lowletter'),
        ];
    }
}
